# -*- coding: utf-8 -*-
"""
Created on Tue Apr 24 18:53:17 2018

@author: jonas
"""

def initialize(context):
    context.security = symbol('AMZN')
    schedule_function(func=action_func,
                      date_rule=date_rules.every_day(),
                      time_rule=time_rules.market_open(hours=0, minutes=1)
                     )
                      
def action_func(context, data): 
    price_history = data.history(assets = context.security,
                                 fields = 'price',
                                 bar_count = 1,
                                 frequency ='1d')
    average_price = price_history.mean()
    current_price = data.current(assets=context.security,
                                 fields = 'price'
                                )
    if data.can_trade(context.security):
        if current_price > average_price * 1.02:
            order_target_percent(context.security,1)
        else:
            order_target_percent(context.security,0)
            
#Changes